/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \file common/data.hpp
 * Static data container.
 */

#ifndef COMMON_DATA_HPP
#define COMMON_DATA_HPP

#include <boost/operators.hpp>
#include <iterator>
#include <cstddef>

namespace common {

/**
 * Read-only Random Access Container (its iterator is not an Output Iterator)
 * for accessing raw, static data.
 *
 * Intended for use with \c boost::iostreams::mapped_file or such.
 */
template <typename T>
struct data : public boost::equality_comparable< data<T> >,
              public boost::less_than_comparable< data<T>, data<T> >
{
	/** Undocumented type */
	typedef T value_type;

	/** Undocumented type */
	typedef const T * iterator;

	/** Undocumented */
	typedef iterator const_iterator;

	/** Undocumented */
	typedef std::reverse_iterator<iterator> reverse_iterator;

	/** Undocumented */
	typedef reverse_iterator const_reverse_iterator;

	/** Undocumented */
	typedef const T & reference;

	/** Undocumented */
	typedef reference const_reference;

	/** Undocumented */
	typedef const T * pointer;

	/** Undocumented */
	typedef std::ptrdiff_t difference_type;

	/** Undocumented */
	typedef unsigned int size_type;

	/** Undocumented */
	data();

	/** Undocumented */
	data(pointer data, size_type size);

	/** Undocumented */
	template <typename MappedFile>
	explicit data(const MappedFile & store);

	/** Undocumented */
	template <typename MappedFile>
	data(const MappedFile & store, size_type size, std::size_t offset = 0);

	/** Undocumented */
	data(const data & other);

	/** Undocumented */
	void reset(pointer data, size_type size);

	/** Undocumented */
	template <typename MappedFile>
	void reset(const MappedFile & store);

	/** Undocumented */
	template <typename MappedFile>
	void reset(const MappedFile & store, size_type size, std::size_t offset = 0);

	/** Undocumented */
	data & operator=(const data & other);

	/** Undocumented */
	bool operator==(const data & other) const;

	/** Undocumented */
	bool operator<(const data & other) const;

	/** Undocumented */
	iterator begin() const;

	/** Undocumented */
	iterator end() const;

	/** Undocumented */
	reverse_iterator rbegin() const;

	/** Undocumented */
	reverse_iterator rend() const;

	/** Get a reference to the element at \p index */
	reference operator[](size_type index) const;

	/** Get the current size */
	size_type size() const;

	/** Get the maximum possible size */
	size_type max_size() const;

	/** Check if the size is zero */
	bool empty() const;

	/** Swap the backing store with the \p other container */
	void swap(data & other);

protected:
	size_type Size;
	pointer Data;
};

/**
 * Swap the backing stores of two read-only data containers.
 */
template <typename T>
void swap(data<T> &, data<T> &);

} // common

#include "data.tpp"
#endif
