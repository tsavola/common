/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \file common/visibility.hpp
 * Portable function visibility specification.
 *
 * \par Examples
 * \code
 * void COMMON_VISIBILITY_HIDDEN foo();
 *
 * struct COMMON_VISIBILITY_HIDDEN bar
 * {
 * };
 * \endcode
 */

#ifndef COMMON_VISIBILITY_HPP
#define COMMON_VISIBILITY_HPP

/**
 * Declare the following symbol as private to the shared object (if supported
 * by compiler).
 */
#ifdef __GNUC__
# define COMMON_VISIBILITY_HIDDEN __attribute__ ((visibility ("hidden")))
#else
# define COMMON_VISIBILITY_HIDDEN
#endif

#endif
