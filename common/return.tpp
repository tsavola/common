/*
 * Copyright 2005-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#ifndef __GNUC__
# include <boost/assert.hpp>
#endif

namespace common {

#ifdef __GNUC__

template <typename T>
inline return_value<T>::return_value(const T & value) :
	Value(value)
{
}

template <typename T>
inline return_value<T>::return_value(const return_value<T> & value) :
	Value(value.Value)
{
}

template <typename T>
inline return_value<T>::operator T() const
{
	return Value;
}

template <typename T>
inline void ignore(const return_value<T> &)
{
}

#else

template <typename T>
inline return_value<T>::return_value(T const & value) :
	Value(value),
	Read(false)
{
}

template <typename T>
inline return_value<T>::return_value(return_value<T> const & value) :
	Value(value.Value),
	Read(value.Read)
{
	value.Read = true;
}

template <typename T>
inline return_value<T>::~return_value()
{
	BOOST_ASSERT(Read);
}

template <typename T>
inline return_value<T>::operator T() const
{
	Read = true;
	return Value;
}

template <typename T>
inline void ignore(return_value<T> const & value)
{
	T dummy = value;
}

#endif

} // common
