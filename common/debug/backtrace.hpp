/*
 * Copyright 2005-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \file common/debug/backtrace.hpp
 * Run-time access to stack trace information.
 */

#ifndef COMMON_DEBUG_BACKTRACE_HPP
#define COMMON_DEBUG_BACKTRACE_HPP

#include <vector>
#include <utility>
#include <string>
#include <iosfwd>

namespace common {

/**
 * Stack frame.  A  pair of function symbol name (\c  first) and dynamic object
 * name (\c second).
 */
typedef std::pair<std::string, std::string> backtrace_element;

/**
 * Stack trace container.
 */
typedef std::vector<backtrace_element> backtrace_container;

/**
 * Get stack trace.
 *
 * \post Returned container will be empty if backtrace could or would not be
 *       generated.
 */
backtrace_container get_backtrace();

/**
 * Get stack trace and print it to \p stream.
 *
 * \return \c true on success.
 */
bool print_backtrace(std::ostream & stream);

/**
 * Print \p backtrace to \p stream.
 */
void print_backtrace(std::ostream & stream,
                     backtrace_container const & backtrace);

} // common

#include "backtrace.tpp"
#endif
