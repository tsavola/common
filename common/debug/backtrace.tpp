/*
 * Copyright 2005-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

namespace common {

#if defined(__GNUC__) && !defined(NDEBUG)

void get_backtrace(backtrace_container &);

inline backtrace_container get_backtrace()
{
	backtrace_container bt;
	get_backtrace(bt);
	return bt;
}

#else

inline void get_backtrace(backtrace_container &)
{
}

inline backtrace_container get_backtrace()
{
	backtrace_container bt;
	return bt;
}

inline bool print_backtrace(std::ostream &)
{
	return false;
}

inline void print_backtrace(std::ostream &, backtrace_container const &)
{
}

#endif

} // common
