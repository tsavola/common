/*
 * Copyright 2004-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \file common/debug/assert.cpp
 * Implements a \c BOOST_ASSERT handler.
 */

#include "backtrace.hpp"
#include <boost/assert.hpp>
#include <iostream>
#include <cstdlib>

namespace boost {

/**
 * Print \p file, \p line and \p expression to \c std::cerr.  Print also the
 * full function call trace (if a backtrace implementation is available) or the
 * \p function name.
 *
 * Used when \c BOOST_ENABLE_ASSERT_HANDLER is defined.
 */
void assertion_failed(char const * expression, char const * function,
                      char const * file, long line)
{
	std::cerr << file << ":" << line << ": assertion (" << expression
	          << ") failed in:" << std::endl;

	if (!common::debug::print_backtrace(std::cerr))
		std::cerr << "\t" << function << std::endl;

	std::abort();
}

} // boost
