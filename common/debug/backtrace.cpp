/*
 * Copyright 2004-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#ifdef NDEBUG
# error NDEBUG defined
#endif

#ifndef __GNUC__
# error GNU toolchain required
#endif

#include "backtrace.hpp"
#include <cstddef>
#include <ostream>
#include <cstring>
#include <cstdlib>
#include <execinfo.h>
#include <cxxabi.h>

namespace common {

void get_backtrace(backtrace_container & bt)
{
	std::size_t size = 64;

	void ** buffer = new void *[size];
	size = ::backtrace(buffer, size);

	char ** strings = ::backtrace_symbols(buffer, size);

	for (std::size_t i = 0; i < size; i++) {
		char * beg = std::strchr(strings[i], '(');
		if (!beg)
			continue;

		*beg = '\0';
		beg++;

		char * end = std::strchr(beg, '+');
		if (!end) {
			end = std::strrchr(beg, ')');
			if (!end)
				continue;
		}

		*end = '\0';

		char * sym = abi::__cxa_demangle(beg, 0, 0, 0);
		if (sym) {
			std::string s = sym;
			std::free(sym);

			bt.push_back(backtrace_element(s, strings[i]));
		}
		else {
			bt.push_back(backtrace_element(beg, strings[i]));
		}
	}
}

bool print_backtrace(std::ostream & out)
{
	backtrace_container bt = get_backtrace();
	if (bt.empty())
		return false;

	print_backtrace(out, bt);
	return true;
}

void print_backtrace(std::ostream & out, const backtrace_container & bt)
{
	backtrace_container::const_iterator i;

	for (i = bt.begin(); i != bt.end(); ++i)
		out << "\t" << i->first << " in " << i->second << std::endl;
}

} // common
