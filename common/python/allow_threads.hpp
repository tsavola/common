/*
 * Copyright 2008  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#ifndef COMMON_PYTHON_ALLOWTHREADS_HPP
#define COMMON_PYTHON_ALLOWTHREADS_HPP

#include <boost/python.hpp>

#include <Python.h>

namespace common {

template <typename Base = boost::python::default_call_policies>
class allow_threads : public Base {
public:
	bool precall(PyObject *args)
	{
		bool result = Base::precall(args);

		if (result)
			state = ::PyEval_SaveThread();

		return result;
	}

	PyObject *postcall(PyObject *args, PyObject *result)
	{
		::PyEval_RestoreThread(state);

		return Base::postcall(args, result);
	}

private:
	PyThreadState *state;
};

} // namespace common

#endif
