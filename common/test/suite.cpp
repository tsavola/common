/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "suite.hpp"

using boost::unit_test::test_suite;

namespace common {
namespace test {

test_suite & suite()
{
	static test_suite * instance = 0;

	if (instance == 0)
		instance = BOOST_TEST_SUITE();

	return *instance;
}

} // test
} // common

test_suite * init_unit_test_suite(int, char **)
{
	return &common::test::suite();
}
