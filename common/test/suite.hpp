/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \file common/test/suite.hpp
 * Test suite singleton.
 */

#ifndef COMMON_TEST_SUITE_HPP
#define COMMON_TEST_SUITE_HPP

#include <boost/test/unit_test_suite.hpp>

namespace common {
namespace test {

/**
 * Get (and possibly initialize) the unit test suite singleton.
 */
boost::unit_test::test_suite & suite();

} // test
} // common

#endif
