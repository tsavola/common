/*
 * Copyright 2004-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \file common/value.hpp
 * A value type.
 */

#ifndef COMMON_VALUE_HPP
#define COMMON_VALUE_HPP

#include <boost/mpl/if.hpp>
#include <boost/mpl/bool.hpp>
#include <boost/integer.hpp>

namespace common {

/**
 * An integer type that enforces lower and upper bounds.  Implements \em
 * Assignable requirements.  The valid value range is \f$[Start,End[\f$.  The
 * fastest and smallest primitive type that can represent the range is used for
 * storage.
 *
 * \param Start smallest value; inclusive start of range.
 * \param End   largest value plus one; exclusive end of range.
 * \param Throw if \c true, throw a \c std::range_error; if \c false, use
 *              \c BOOST_ASSERT .
 */
template <int Start, int End = Start + 1, bool Throw = false>
struct value
{
	/** The primitive storage type */
	typedef typename boost::mpl::if_<
	        boost::mpl::bool_<(Start < 0)>,
	                          typename boost::mpl::if_<
	                                  boost::mpl::bool_<(-End > Start)>,
	                          boost::int_min_value_t<Start>,
	                          boost::int_max_value_t<End>
	                          >::type,
	        boost::uint_value_t<End>
	        >::type::fast data_type;

	/** Initialize to \f$ max(0,Start) \f$ */
	value();

	/** Explicit initialization */
	template <typename T>
	value(const T &);

	/** Represent the value as a primitive type */
	operator data_type() const;

	/** Swap value with \p other */
	void swap(value & other);

	/** Integer value semantics \{ */
	template <typename T> value<Start,End,Throw> & operator=(const T &);
	template <typename T> value<Start,End,Throw> & operator+=(const T &);
	template <typename T> value<Start,End,Throw> & operator-=(const T &);
	template <typename T> value<Start,End,Throw> & operator*=(const T &);
	template <typename T> value<Start,End,Throw> & operator/=(const T &);
	template <typename T> value<Start,End,Throw> & operator%=(const T &);
	template <typename T> value<Start,End,Throw> & operator|=(const T &);
	template <typename T> value<Start,End,Throw> & operator&=(const T &);
	template <typename T> value<Start,End,Throw> & operator^=(const T &);

	value<Start,End,Throw> & operator++();
	value<Start,End,Throw> & operator--();
	value<Start,End,Throw> operator++(int);
	value<Start,End,Throw> operator--(int);
	/** \} */

private:
	template <typename T>
	value<Start,End,Throw> & set(const T &);

	data_type Value;
};

/**
 * Swap two values.
 */
template <int Start, int End, bool Throw>
void swap(value<Start,End,Throw> &, value<Start,End,Throw> &);

} // common

#include "value.tpp"
#endif
