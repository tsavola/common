/*
 * Copyright 2005-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include <boost/integer/static_min_max.hpp>
#include <boost/integer_traits.hpp>
#include <boost/mpl/if.hpp>
#include <boost/assert.hpp>
#include <stdexcept>
#include <algorithm>

namespace common {

template <int Start, int End, bool Throw> struct value_check;

template <int Start, int End>
struct value_check<Start,End,true>
{
	static void check(int value)
	{
		if (value < Start)
			throw std::range_error("value is too small");
		if (value >= End)
			throw std::range_error("value is too large");
	}
};

template <int Start, int End>
struct value_check<Start,End,false>
{
#ifndef BOOST_DISABLE_ASSERTS
	static void check(int value)
	{
		BOOST_ASSERT(value >= Start);
		BOOST_ASSERT(value < End);
	}
#else
	static void check(int)
	{
	}
#endif
};

template <int Start, int End, bool Throw>
inline value<Start,End,Throw>::value() :
	Value(boost::static_signed_max<0,Start>::value)
{
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw>::value(const T & i)
{
	set(i);
}

/**
 * Checks that \p i is within the range \f$ [Start,End[ \f$ and stores it.
 */
template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::set(const T & i)
{
	value_check<Start,End,Throw>::check(int(i));
	Value = i;
	return *this;
}

template <int Start, int End, bool Throw>
inline value<Start,End,Throw>::operator data_type() const
{
	return Value;
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator=(const T & i)
{
	return set(i);
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator+=(const T & i)
{
	return set(Value + i);
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator-=(const T & i)
{
	return set(Value - i);
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator*=(const T & i)
{
	return set(Value * i);
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator/=(const T & i)
{
	return set(Value / i);
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator%=(const T & i)
{
	return set(Value % i);
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator|=(const T & i)
{
	return set(Value | i);
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator&=(const T & i)
{
	return set(Value & i);
}

template <int Start, int End, bool Throw>
template <typename T>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator^=(const T & i)
{
	return set(Value ^ i);
}

template <int Start, int End, bool Throw>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator++()
{
	return set(Value + 1);
}

template <int Start, int End, bool Throw>
inline value<Start,End,Throw> & value<Start,End,Throw>::operator--()
{
	return set(Value - 1);
}

template <int Start, int End, bool Throw>
inline value<Start,End,Throw> value<Start,End,Throw>::operator++(int)
{
	data_type t = Value;
	set(Value + 1);
	return t;
}

template <int Start, int End, bool Throw>
inline value<Start,End,Throw> value<Start,End,Throw>::operator--(int)
{
	data_type t = Value;
	set(Value - 1);
	return t;
}

template <int Start, int End, bool Throw>
inline void value<Start,End,Throw>::swap(value & other)
{
	using std::swap;

	swap(Value, other.Value);
}

template <int Start, int End, bool Throw>
inline void swap(value<Start,End,Throw> & a, value<Start,End,Throw> & b)
{
	a.swap(b);
}

} // common
