/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \file common/construction.hpp
 * Portable constructor/destructor functions
 *
 * \par Examples
 * \code
 * COMMON_CONSTRUCTOR(foo)
 * {
 *         get_some_singleton().push_back("foo");
 * }
 *
 * COMMON_DESTRUCTOR(foo)
 * {
 *         some_container const & x = get_some_singleton();
 *         if (x.find("foo") == x.end())
 *                 cerr << "Warning: \"foo\" was not processed" << endl;
 * }
 * \endcode
 */

#ifndef COMMON_CONSTRUCTION_HPP
#define COMMON_CONSTRUCTION_HPP

namespace common {

#ifdef __GNUC__

# define COMMON_CONSTRUCTOR(name) \
	static void __attribute__ ((constructor)) name##__constructor__(void) \
		throw ()

# define COMMON_DESTRUCTOR(name) \
	static void __attribute__ ((destructor)) name##__destructor__(void) \
		throw ()

#else

/**
 * Run code automatically during initialization (before \p main).
 *
 * \param Name is a symbolic constructor name which must be unique within the
 *             compilation unit.
 */
# define COMMON_CONSTRUCTOR(Name) \
	static void Name##__constructor__(void) throw (); \
	static ::common::construction Name##__construction__(\
		Name##__constructor__); \
	static void Name##__constructor__(void) throw ()

/**
 * Run code automatically at program termination.
 *
 * \param Name is a symbolic destructor name which must be unique within the
 *             compilation unit.
 */
# define COMMON_DESTRUCTOR(Name) \
	static void Name##__destructor__(void) throw (); \
	static ::common::destruction Name##__destruction__( \
		Name##__destructor__); \
	static void Name##__destructor__(void) throw ()

struct construction
{
	construction(void (*)(void)) throw ();
};

struct destruction
{
	destruction(void (*)(void)) throw ();
	~destruction() throw ();

private:
	void (* const function)(void);
};

inline construction::construction(void (* constructor)(void))
	throw ()
{
	constructor();
}

inline destruction::destruction(void (* destructor)(void))
	throw () :
	function(destructor)
{
}

inline destruction::~destruction()
	throw ()
{
	function();
}

#endif

} // common

#endif
