/*
 * Copyright 2005-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

/**
 * \file common/return.hpp
 * Portable return value checking.
 *
 * Reimplementation of the ErrorCode class described by Andrei Alexandrescu on
 * page 51 of the C/C++ Users Journal February 2005 issue.
 *
 * When compiling with GCC, the check is done at compile time.  Otherwise
 * portable run-time checking is used.
 *
 * \par Examples
 * \code
 * COMMON_CHECK_RETURN(int) foo();
 *
 * COMMON_CHECK_RETURN(int) foo()
 * {
 *         return -1;
 * }
 * \endcode
 * \code
 * void client()
 * {
 *         foo();                 // Bad
 *
 *         if (foo() < 0) throw;  // Good
 *
 *         ignore(foo());         // Good
 * }
 * \endcode
 */

#ifndef COMMON_RETURN_HPP
#define COMMON_RETURN_HPP

namespace common {

template <typename T> struct return_value;

/**
 * Mandate the reading of a function's return value.
 *
 * \param T is the return type in a function signature.
 */
#ifdef __GNUC__
# define COMMON_CHECK_RETURN(T) \
	::common::return_value<T> __attribute__ ((warn_unused_result))
#else
# define COMMON_CHECK_RETURN(T) \
	::common::return_value<T>
#endif

/**
 * Discard a return value.
 */
template <typename T>
void ignore(return_value<T> const &);

/**
 * Return value wrapper.
 */
template <typename T>
struct return_value
{
	friend void ignore<T>(return_value const &);

	return_value(T const &);
	return_value(return_value<T> const &);
	operator T() const;

#ifndef __GNUC__
	~return_value();
#endif

private:
	T Value;

#ifndef __GNUC__
	mutable bool Read;
#endif
};

} // common

#include "return.tpp"
#endif
