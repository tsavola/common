/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include <boost/assert.hpp>
#include <boost/static_assert.hpp>
#include <algorithm>
#include <cstddef>
#include <limits>
#include <stdexcept>

namespace common {

template <typename T>
inline data<T>::data()
{
	Size = 0;
	Data = NULL;
}

template <typename T>
inline data<T>::data(pointer data, size_type size)
{
	reset(data, size);
}

template <typename T>
template <typename MappedFile>
inline data<T>::data(const MappedFile & s)
{
	reset(s);
}

template <typename T>
template <typename MappedFile>
inline data<T>::data(const MappedFile & s, size_type size, std::size_t offset)
{
	reset(s, size, offset);
}

template <typename T>
inline data<T>::data(const data & v)
{
	operator=(v);
}

template <typename T>
inline void data<T>::reset(pointer data, size_type size)
{
	Size = size;
	Data = data;
}

template <typename T>
template <typename MappedFile>
void data<T>::reset(const MappedFile & s)
{
	BOOST_STATIC_ASSERT(sizeof (typename MappedFile::char_type) == 1);
	const typename MappedFile::char_type * data = s.data();

	size_type raw_size = s.size();
	Size = raw_size / sizeof (T);

	if (Size * sizeof (T) != raw_size)
		throw std::range_error("Back-end size alignment mismatch");

	Data = reinterpret_cast<pointer> (data);
}

template <typename T>
template <typename MappedFile>
void data<T>::reset(const MappedFile & s, size_type size, std::size_t offset)
{
	BOOST_STATIC_ASSERT(sizeof (typename MappedFile::char_type) == 1);
	const typename MappedFile::char_type * data = s.data();

	if (offset + size * sizeof (T) > s.size())
		throw std::range_error("Back-end size exceeded");

	Size = size;
	Data = reinterpret_cast<pointer> (data + offset);
}

template <typename T>
inline data<T> & data<T>::operator=(const data & v)
{
	Size = v.Size;
	Data = v.Data;

	return *this;
}

template <typename T>
bool data<T>::operator==(const data & v) const
{
	if (this == &v)
		return true;

	if (size() != v.size())
		return false;

	if (Data == v.Data)
		return true;

	if (empty())
		return true;

	return std::equal(begin(), end(), v.begin());
}

template <typename T>
bool data<T>::operator<(const data & v) const
{
	if (this == &v)
		return false;

	if (size() == v.size() && Data == v.Data)
		return false;

	if (empty() && v.empty())
		return false;

	return std::lexicographical_compare(begin(), end(),
	                                    v.begin(), v.end());
}

template <typename T>
inline typename data<T>::iterator data<T>::begin() const
{
	return Data;
}

template <typename T>
inline typename data<T>::iterator data<T>::end() const
{
	return Data + Size;
}

template <typename T>
inline typename data<T>::reverse_iterator data<T>::rbegin() const
{
	return reverse_iterator(end());
}

template <typename T>
inline typename data<T>::reverse_iterator data<T>::rend() const
{
	return reverse_iterator(begin());
}

template <typename T>
inline typename data<T>::reference data<T>::operator[](size_type i) const
{
	BOOST_ASSERT(i < Size);
	return Data[i];
}

template <typename T>
inline typename data<T>::size_type data<T>::size() const
{
	return Size;
}

template <typename T>
inline typename data<T>::size_type data<T>::max_size() const
{
	return Size;
}

template <typename T>
inline bool data<T>::empty() const
{
	return Size == 0;
}

template <typename T>
void data<T>::swap(data & v)
{
	std::swap(Size, v.Size);
	std::swap(Data, v.Data);
}

template <typename T>
inline void swap(data<T> & a, data<T> & b)
{
	a.swap(b);
}

} // common
