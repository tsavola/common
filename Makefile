# Copyright 2006-2007  Timo Savola
# Distributed under the Boost Software License, Version 1.0.

-include BuildOptions.mk
include $(BUILD)/build.mk

# Libraries

common_debug_NAME		= common-debug
common_debug_VERSION		= 1
common_debug_SOURCES		= $(wildcard common/debug/*.cpp)

$(call library,common_debug)

common_test_NAME		= common-test
common_test_VERSION		= 1
common_test_SOURCES		= $(wildcard common/test/*.cpp)
common_test_LIBS		= -lboost_unit_test_framework

$(call library,common_test)

# Test helper

export BOOST_TEST_REPORT_LEVEL=no

define do_common_test
test_$(1)_NAME			?= test-$$(subst _,-,$(1))
test_$(1)_SOURCES		?= test/$(1).cpp
test_$(1)_CPPFLAGS_debug	?= -DBOOST_ENABLE_ASSERT_HANDLER
test_$(1)_LIBS_debug		?= -lcommon-debug
test_$(1)_DEPENDS_debug		?= common_debug@static

$$(call test,test_$(1))
endef

common_test = $(eval $(call do_common_test,$(1)))

# Tests

test_suite_SOURCES		= test/data.cpp test/value.cpp
test_suite_LIBS			= -lcommon-test -lboost_iostreams
test_suite_DEPENDS		= common_test@shared

$(call common_test,suite)

$(call common_test,construction)

test_return_gnu_SOURCES		= test/return.cpp

$(call common_test,return_gnu)

test_return_portable_SOURCES	= test/return.cpp
test_return_portable_CPPFLAGS	= -DPORTABLE

$(call common_test,return_portable)

# Tags

TAGS_SOURCES	= $(wildcard common/*.hpp) $(wildcard common/*/*.hpp)

# Build

build: common_debug-debug-static common_test-optimize-shared

# Install

install: build
	install -d $(DESTDIR)$(PREFIX)/include/common/debug
	install -d $(DESTDIR)$(PREFIX)/include/common/test

	install -m 644 common/*.[ht]pp \
	               $(DESTDIR)$(PREFIX)/include/common/
	install -m 644 common/debug/*.[ht]pp \
	               $(DESTDIR)$(PREFIX)/include/common/debug/
	install -m 644 common/test/*.[ht]pp \
	               $(DESTDIR)$(PREFIX)/include/common/test/

	install -d $(DESTDIR)$(PREFIX)/lib

	install -m 644 $(common_debug_STATIC_LIBRARY_debug) \
	               $(DESTDIR)$(PREFIX)/lib/

	install -m 644 $(common_test_SHARED_LIBRARY_optimize) \
	               $(DESTDIR)$(PREFIX)/lib/
	ln -sf $(common_test_SHARED_LIBRARY_NAME) \
	       $(DESTDIR)$(PREFIX)/lib/$(common_test_SHARED_GENERIC_NAME)
