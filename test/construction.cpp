/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include <common/construction.hpp>
#include <iostream>

#ifdef __GNUC__

COMMON_CONSTRUCTOR(test1)
{
	std::cout << "GNU constructor" << std::endl;
}

COMMON_DESTRUCTOR(test1)
{
	std::cout << "GNU destructor" << std::endl;
}

# undef __GNUC__
#endif

COMMON_CONSTRUCTOR(test2)
{
	std::cout << "Portable constructor" << std::endl;
}

COMMON_DESTRUCTOR(test2)
{
	std::cout << "Portable destructor" << std::endl;
}

int main()
{
	std::cout << "Main" << std::endl;
	return 0;
}
