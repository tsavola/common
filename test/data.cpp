/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include <common/data.hpp>
#include <common/construction.hpp>
#include <common/test/suite.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/cstdint.hpp>
#include <iostream>
#include <stdexcept>

namespace {

using namespace common;
using boost::iostreams::mapped_file_source;

template <typename T>
void size()
{
	using std::cout;
	using std::endl;

	const mapped_file_source f("/etc/issue", 32);
	data<T> v(f);

	cout << v.size() << endl;
}

void bad_size()
{
	try {
		const mapped_file_source f("/etc/issue", 11);
		data<uint32_t> v(f);

		BOOST_CHECK(false);

	} catch (std::range_error &) {
	}

}

template <typename T, int Size, int Offset>
void iterate()
{
	using std::cout;
	using std::endl;

	const mapped_file_source f("/etc/issue");
	data<T> v(f, Size, Offset);

	for (typename data<T>::iterator i = v.begin(); i != v.end(); ++i)
		cout << *i << ' ';

	cout << endl;
}

template <typename T, int Size, int Offset>
void access()
{
	using std::cout;
	using std::endl;

	const mapped_file_source f("/etc/issue");
	data<T> v(f, Size, Offset);

	for (size_t i = 0; i < v.size(); i++)
		cout << v[i] << ' ';

	cout << endl;
}

template <typename T, int Size, int Offset>
void reverse_iterate()
{
	using std::cout;
	using std::endl;

	const mapped_file_source f("/etc/issue");
	data<T> v(f, Size, Offset);

	typename data<T>::reverse_iterator i;
	for (i = v.rbegin(); i != v.rend(); ++i)
		cout << *i << ' ';

	cout << endl;
}

template <typename T>
void compare()
{
	const mapped_file_source f("/etc/motd", 128);
	const mapped_file_source g("/etc/motd", 128);
	const mapped_file_source h("/etc/issue", 16);

	const data<T> a(f);
	const data<T> b(f);

	BOOST_CHECK(a == b);
	BOOST_CHECK(!(a != b));

	const data<T> c = a;

	BOOST_CHECK(a == c);

	const data<T> d(h);

	BOOST_CHECK(a != d);

	const data<T> x(f, 8, 2);
	const data<T> y(g, 8, 2);
	const data<T> z = x;

	BOOST_CHECK(a != x);
	BOOST_CHECK(x == y);
	BOOST_CHECK(x == z);
}

COMMON_CONSTRUCTOR(init)
{
	using boost::uint8_t;
	using boost::uint16_t;
	using boost::uint32_t;
	using boost::uint64_t;

	boost::unit_test::test_suite & s = common::test::suite();

	s.add(BOOST_TEST_CASE(&( size<uint8_t> )));
	s.add(BOOST_TEST_CASE(&( size<uint16_t> )));
	s.add(BOOST_TEST_CASE(&( size<uint32_t> )));
	s.add(BOOST_TEST_CASE(&( size<uint64_t> )));

	s.add(BOOST_TEST_CASE(&( bad_size )));

	s.add(BOOST_TEST_CASE(&( iterate<char, 6, 0> )));
	s.add(BOOST_TEST_CASE(&( iterate<uint32_t, 2, 7> )));

	s.add(BOOST_TEST_CASE(&( reverse_iterate<char, 9, 7> )));

	s.add(BOOST_TEST_CASE(&( access<char, 6, 0> )));
	s.add(BOOST_TEST_CASE(&( access<uint32_t, 2, 7> )));

	s.add(BOOST_TEST_CASE(&( compare<uint16_t> )));
	s.add(BOOST_TEST_CASE(&( compare<double> )));
}

} // anonymous
