/*
 * Copyright 2006-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#if defined(PORTABLE) && defined(__GNUC__)
# undef __GNUC__
#endif

#include <common/return.hpp>

struct structure
{
	int i, j;
};

class object
{
public:
	object() :
		i(0)
	{
	}

	object(object const & o) :
		i(o.i)
	{
	}

	virtual ~object()
	{
	}

	virtual object & operator=(object const & o)
	{
		i = o.i;
	}

protected:
	int i;
};

COMMON_CHECK_RETURN(int) get_integer()
{
	return 0;
}

COMMON_CHECK_RETURN(void *) get_pointer()
{
	return 0;
}

COMMON_CHECK_RETURN(structure) get_structure()
{
	structure x;
	return x;
}

COMMON_CHECK_RETURN(object) get_object()
{
	object x;
	return x;
}

int main()
{
	int i = get_integer();
	void * p = get_pointer();
	structure s = get_structure();
	object o = get_object();

	ignore(get_integer());
	ignore(get_pointer());
	ignore(get_structure());
	ignore(get_object());

	return 0;
}
