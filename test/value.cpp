/*
 * Copyright 2005-2007  Timo Savola
 * Distributed under the Boost Software License, Version 1.0.
 */

#include <common/value.hpp>
#include <common/construction.hpp>
#include <common/test/suite.hpp>
#include <boost/test/test_tools.hpp>

namespace {

using common::value;

void value_parameters()
{
	BOOST_CHECK(typeid (value<10>) == typeid (value<10, 10+1>));
	BOOST_CHECK(typeid (value<10, 20>) == typeid (value<10, 20, false>));
}

void value_size()
{
	BOOST_CHECK(sizeof (value<0, 0xff>::data_type) >= 1);
	BOOST_CHECK(sizeof (value<0, 0x100>::data_type) > 1);
	BOOST_CHECK(sizeof (value<0, 0xffff>::data_type) >= 2);
	BOOST_CHECK(sizeof (value<0, 0x10000>::data_type) > 2);
	BOOST_CHECK(sizeof (value<0, 0xffffffff>::data_type) >= 4);

	BOOST_CHECK(sizeof (value<-0x80, 0>::data_type) >= 1);
	BOOST_CHECK(sizeof (value<-0x81, 0>::data_type) > 1);
	BOOST_CHECK(sizeof (value<-0x8000, 0>::data_type) >= 2);
	BOOST_CHECK(sizeof (value<-0x8001, 0>::data_type) > 2);
	BOOST_CHECK(sizeof (value<-0x80000000, 0>::data_type) >= 4);

	BOOST_CHECK(sizeof (value<-0x80, 0x80>::data_type) > 1);
	BOOST_CHECK(sizeof (value<-0x8000, 0x8000>::data_type) > 2);
}

template <int Start, int End, int Value1, int Value2>
void value_operator_add()
{
	const value<Start, End> i = Value1;
	const value<Start, End> j = Value2;
	const value<Start, End> r = Value1 + Value2;

	BOOST_CHECK(i + j == r);
}

COMMON_CONSTRUCTOR(init)
{
	boost::unit_test::test_suite & s = common::test::suite();

	s.add(BOOST_TEST_CASE( &value_parameters ));
	s.add(BOOST_TEST_CASE( &value_size ));
	s.add(BOOST_TEST_CASE( &(value_operator_add<0, 100, 50, 40>) ));
	s.add(BOOST_TEST_CASE( &(value_operator_add<-100, 0, -50, -40>) ));
}

} // anonymous
