# Copyright 2006-2007  Timo Savola
# Distributed under the Boost Software License, Version 1.0.

CPPFLAGS_optimize	+= -DBOOST_DISABLE_ASSERTS
CXXFLAGS_optimize	+= -std=c++98 -pedantic -Wall -Wno-unused
