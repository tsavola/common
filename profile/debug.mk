# Copyright 2006-2007  Timo Savola
# Distributed under the Boost Software License, Version 1.0.

CPPFLAGS_debug		+= -DBOOST_ENABLE_ASSERT_HANDLER
CXXFLAGS_debug		+= -std=c++98 -pedantic -Wall -Wextra
LIBS_debug		+= -lcommon-debug
DEPENDS_debug		+= common_debug@static
