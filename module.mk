# Copyright 2006-2007  Timo Savola
# Distributed under the Boost Software License, Version 1.0.

COMMON_TARGET		= $(TARGET)
COMMON_TARGETDIR	= $(COMMON)/$(TARGETBASE)/$(COMMON_TARGET)

CPPFLAGS		+= -I$(COMMON)

TAGS_INCLUDES		+= $(COMMON)

$(call profiles,$(COMMON)/profile/*.mk)

common_debug_NAME	= common-debug
common_debug_TARGETDIR	= $(COMMON_TARGETDIR)

$(call export_library,common_debug)

common_test_NAME	= common-test
common_test_TARGETDIR	= $(COMMON_TARGETDIR)

$(call export_library,common_test)
